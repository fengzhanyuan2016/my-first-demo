FROM java:8-jre-alpine
WORKDIR /app
ADD . .
ENTRYPOINT ["java","-jar","demo-0.0.1-SNAPSHOT.jar"]